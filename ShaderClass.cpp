#include "ShaderClass.h"
#include "ResourceManager.h"
#include "GameController.h"

ShaderClass::ShaderClass(string name , const char * vs , const char * fs) 
{
	this->name = name ;
	new Shader(name , vs , fs) ; // register
}
	