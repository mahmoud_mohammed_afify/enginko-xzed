graphics engine designed to work on any API initially implemented on openGL, it was developed for simulation and game development purposes.


the main modules:-

Platform manager: defines the API with functions.

Math module: matrix and vector operations, also included set of special matrices that’s used in transformations.

Game controller: manage the workflow of the game.

Rendering manager: controls the rendering process and the drawing techniques.

Resource manager: shaders, textures and materials, it’s responsible to load and save them, there’s also available implemented shaders to be used.

UI manager: responsible for the UI creation and event handling.

Using C++ - GLSL