#pragma once
#include "Matrix.h"
#include "Vector3.h"
#include "IObject.h"

class GameObject;

class GameComponent : public IObject
{
public :
	GameComponent(GameObject* parent , Vector3 pos) : IObject(parent , pos){}

};
