#pragma once
#include "ImmedientShapes.h"
#include "GraphicsManager.h"
#include "ModelHelpers.h"

class GeometryVBO :public Shape
{
public:
	GeometryVBO(int numOfVerices, IVertex vertex[], int numOfIndices, int Indices[]);
	void Edit(int start, int end, float arr[]){}
	void Transform(Matrix Transformation){}
	void Draw(){ Draw(Shapes::TRIANGLE); }
	void Debug(){ Draw(Shapes::LINE); }
	void Draw(Shapes);
	void Draw(int numOfShaderAttributes, int shaderAttributesIndices[], Shapes);
	void Draw(int numOfShaderAttributes, int shaderAttributesIndices[]);
	unsigned int GetNumOfVertices() { return numOfVertices; }
	int GetVBOID() { return vbo; }
private:
	unsigned int vbo;
	unsigned int ibo;
	VertexData Mode;
	int vertexSize;
	int numOfVertices;
	int numOfIncices;
	void* Offsets[];
	void DetermineOffset(IVertex *v);


};