#pragma once
#include "Vector3.h"
#include "Matrix.h"
#include "DrawBehavior.h"
#include "UpdateBehavior.h"
#include "DebugManager.h"
class GameObject ;

class IObject
{
public:
	IObject(Vector3 pos )
	{
		position = pos ;
		parent = NULL ;
		updateBehavior = NULL ;
		drawBehavior = NULL ;
	}
	IObject(GameObject* parent , Vector3 pos )
	{
		position = pos ;
		this->parent = parent ;
		updateBehavior = NULL ;
		drawBehavior = NULL ;
	}
	GameObject* GetParent()
	{
		if(!parent)
			DepugManager::ShowMessage("getting Null Parent");
		return parent ;
	}
	Vector3 GetPosition()
	{
		return position ;
	}
	void SetParent(GameObject* x)
	{
		parent = x ;
	}
	Matrix GetTransform()
	{
		return transform ;
	}
	void SetTransform(Matrix m)
	{
		transform = m ;
	}
	UpdateBehavior* GetUpdateBhavior()
	{
		if(!parent)
			DepugManager::ShowMessage("getting Null UpdateBehavior");

		return updateBehavior ;
	}
	void SetUpdateBehavior(UpdateBehavior* update)
	{
		updateBehavior = update ;
	}
	DrawBehavior* GetDrawBhavior()
	{
		if(!parent)
			DepugManager::ShowMessage("getting Null DrawBehavior");

		return drawBehavior ;
	}
	void SetDrawBhavior(DrawBehavior* draw)
	{
		drawBehavior = draw ;
	}
protected:
	Vector3 position ;
	Matrix transform ;
	UpdateBehavior* updateBehavior ;
	DrawBehavior* drawBehavior ;
private:
	GameObject* parent ;
};